#! /bin/python3

import numpy as np
import matplotlib.pyplot as plt

def K(omega, A, eps_star_p, eps_star_m):
    return (eps_star_p(omega) - eps_star_m(omega))/(3 * (eps_star_m(omega) + A *(eps_star_p(omega) - eps_star_m(omega))))

def eps_star(epsilon, sigma):
    return lambda omega : epsilon - 1.j * sigma/omega

A_range = np.linspace(0.01, 1/3, 10)
omega_range = np.logspace(0, 9, 100)

eps_star_m = eps_star(80, 500)
eps_star_adn = eps_star(8e5, 1e12)

fig, ax = plt.subplots(1, 1)
for A in A_range:
    CM = K(omega_range, A, eps_star_adn, eps_star_m)
    ax.plot(omega_range, np.real(CM), label="A={:.2}".format(A))
ax.legend()
ax.set_xscale('log')
ax.grid(which="both",)
fig.savefig("SVG/A_factor.eps")
