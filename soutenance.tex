
\usetheme[progressbar=frametitle, block=fill, subsectionpage=progressbar]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\definecolor{ENSBlue}{RGB}{0, 119, 139}
\definecolor{ENSJaunePorte}{RGB}{255, 180, 50}
\definecolor{ENSBleuCheminee}{RGB}{70, 35, 0}
\definecolor{ENSBrunBois}{RGB}{210, 160, 90}
\definecolor{ENSNoir}{RGB}{0, 0, 0}
\definecolor{ENSVertFenetre}{RGB}{130,  190, 50}
\definecolor{ENSGrisBeton}{RGB}{190, 190, 190}
\definecolor{ENSRougeBrique}{RGB}{240, 130, 90}

\title{Diélectrophorèse et ADN}
\subtitle{Soutenance de rapport de recherche}
\date{\today}
%\date{}
\author{Léopold \textsc{Clément}, Etienne \textsc{Stransky}}
\institute{M2FESUP}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\setbeamercolor{alerted text}{fg=ENSRougeBrique}
\setbeamercolor{exemple text}{fg=ENSVertFenetre}
\setbeamercolor{frametitle}{bg=ENSBlue}


\setbeamercolor{progress bar}{fg=ENSJaunePorte, bg=ENSGrisBeton}

\usepackage{amsmath}
\usepackage{minted}
\usepackage{xspace}

\newcommand{\code}[1]{\mintinline[breaklines, breakafter=.]{python}{#1}}

\begin{document}

\maketitle

\begin{frame}[allowframebreaks]{Sommaire}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents%[hideallsubsections]
\end{frame}

\section{Problématique}

\begin{frame}{Vue d'ensemble}
  \begin{alertblock}{Objectif}
    Utiliser la force de diélectrophorèse pour séparer les molécules d'ADN dans un milieu en fonction de leur taille      
  \end{alertblock}
  \pause

  Sous-problèmes :
  \begin{itemize}
    \item Comprendre la force de diélectrophorèse
    \item Modéliser le comportement électrique de la molécule d'ADN
    \item Construire un protocole expérimental approprié
  \end{itemize}
\end{frame}


\section{État de l'art}

\subsection{Présentation de la DEP}

\begin{frame}{Histoire de la Diélectrophorèse}

  \begin{itemize}
    \item Discussion du principe vers 1920
    \item Définition de l'expression "Diélectrophorèse" en 1951
    \item Augmentation considérable des études à partir de 1990
  \end{itemize}

  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.7\textwidth]{Bitmap/Evolution_publi_DEP.png}
    \caption{Évolution du nombre de publication annuelles portant sur la DEP, \cite{sarno_dielectrophoresis_2021}}
  \end{figure}

\end{frame}

\begin{frame}{Principe général}

  La diélectrophorèse est une force créée par le gradient d'un champ électrique sur une particule non chargée, mais polarisable

  \begin{itemize}
    \item Différente de l'électrophorèse
    \item Nécessite d'avoir un champ non uniforme
  \end{itemize}

  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.5\textwidth]{SVG/illustration_DEP.pdf}
    \caption{Illustration du principe de la diélectrophorèse}
  \end{figure}

\end{frame}

\begin{frame}{Exemples d'applications}

  La diélectrophorèse sert de base à de nombreuses applications
  \begin{itemize}
    \item Isolation et séparation de cellules
    \item Conception de micro-circuits (nanotubes)
    \item Détection de particules dans un milieu
  \end{itemize}

\end{frame}

\subsection{Modélisation de la force de diélectrophorèse}

\begin{frame}{Modèlisation par un dipole}
    On considère une particule sphérique, avec deux charge :
    \begin{equation}
      \left\langle F_{DEP}  \right\rangle = 2 \pi \epsilon_m R^3 CM \left( \nabla E ^ 2 \right)
    \end{equation}
    \pause
    \begin{equation}
      CM = \frac{\epsilon_ p - \epsilon_m}{\epsilon _ p + 2 \cdot \epsilon_m}
    \end{equation}
\end{frame}

\begin{frame}{Modélisation par un n-pole}
    On considère une particule à n-pole:
    \begin{figure}[htp]
      \centering
      \includegraphics[width = 0.5\textwidth]{SVG/n-pole.eps}
      \caption{Exemple de particule n-pole}
    \end{figure}
\end{frame}

\begin{frame}{DEP sur un n-pole}
  On modifie les formules précedante pour représenter les multiples pole :\pause
  \begin{align}
    CM _n &= \frac{\epsilon _ p ^ * - \epsilon _ m ^ *}{n \epsilon _ p ^ * + \left( n + 1 \right) \epsilon _ m ^ *} \\
    p_n &= \frac{4 \pi \epsilon_m E ^ {2n + 1}}{\left(2n - 1\right)!!}CM_n \left( \nabla \right) ^ {n-1} E \\
    \left\langle F_{DEP, n}  \right\rangle &= Re \left[ \frac{p_n \left[ \cdot \right] ^ n \left( \nabla \right) ^ n E}{n !} \right]
  \end{align}
\end{frame}

\begin{frame}{Intération entre les particules}
    Les particules tendent à l'aligner le long des lignes de champs
    \begin{figure}[htp]
      \centering
      \includegraphics[width = 0.5\textwidth]{SVG/alignement_particule.eps}
      \caption{Placement des particules entre deux électrodes, en claire avec une DEP négative, en foncé une DEP positive.}
    \end{figure}
\end{frame}

\begin{frame}{Effet de la proximité des électrodes}
  A proximité des électrodes, il y a des charges induites à la surface de l'électrodes.
  Le ratio entre la force en champs infini et la force dû à l'intéraction est de \begin{equation}
    \left( \frac{L}{h}  \right) \left( \frac{R}{2h} \right) ^ 3
  \end{equation}
\end{frame}

\subsection{Claussius Mossotti pour une particule non-ronde}

\begin{frame}{Claussius Mossotti}
  Pour un particule non sphérique, il faut corriger l'équation de base :
  \begin{equation}
    CM = \frac{\epsilon_ p - \epsilon_m}{ 3 \left( \epsilon_m + A \left( \epsilon _ p  - \epsilon_m \right) \right)}
  \end{equation}
  Le facteur $A$ est un facteur géométrique.
\end{frame}

\begin{frame}{Facteur $A$}
  Le facteur $A$ vaux $\frac{1}{3}$ pour les sphères.
  Il décroit en fonction du ratio des longueur de la particule
  \begin{alertblock}{Conclusion}
    Ainsi, pour de l'ADN :
    \begin{equation*}
      A \left( 10 \text{bases}  \right) > A \left( 100 \text{bases}  \right) > A \left( 1000 \text{bases}  \right)
    \end{equation*}
  \end{alertblock}
\end{frame}

\subsection{L'ADN circulant}

\begin{frame}{De l'ADN circulant librement dans le sang}
  Il y a, dans le sang, de l'ADN ciculant librement (cfDNA). Cela peut être lié à :
  \begin{itemize}
  \item Un cancer
  \item Un traumatisme physique
  \item Une transplantation d'organes
  \end{itemize}

  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.8\textwidth]{SVG/illustration_ADN_circulant.pdf}
    \caption{Illustration de la présence d'ADN circulant dans le sang}
  \end{figure}

\end{frame}

\begin{frame}{L'ADN circulant lié au cancer}
  On observe chez les personnes atteintes de cancer une augmentation de la quantité d'ADN circulant dans le sang \cite{mouliere_enhanced_2018}.

  \begin{itemize}
    \item Raison exacte encore sujet à des études
    \item Modification de la taille des molécules
    \item Possibilité d'études associées à ces molécules
  \end{itemize}

  \begin{figure}[htp]
    \centering
    \includegraphics[width = 0.8\textwidth]{SVG/Taille_ADN_cancer.pdf}
    \caption{Analyse des tailles de molécules d'ADN circulantes pour différents types de cancers, \cite{mouliere_enhanced_2018}}
  \end{figure}
\end{frame}

\section{Limites de la connaissance actuelle}

\begin{frame}{Limitations actuelles liées à l'analyse de l'ADN}

  \begin{itemize}
  \item Destruction de l'ADN par les macrophages
  \item Forte disparité entre les patients
  \item Peu de corrélation entre la gravité du cancer et la concentration d'ADN
  \end{itemize}
  
  \pause
  \begin{alertblock}{Conclusion}
    Il y a de nombreuses pistes à explorer pour mieux étudier l'ADN circulant dans le sang      
  \end{alertblock}
\end{frame}


\begin{frame}{Polarisabilité de l'ADN}
    \cite{viefhues_dna_2017} rappelle que la modélidation de l'ADN est imparfaite :
    \begin{itemize}
      \item Polarisation des bases,
      \item Role des contre-ions,
      \item Mobilité des contre-ions,
      \item Intéraction entre les bases\dots
    \end{itemize}
\end{frame}

\begin{frame}{Limites de la modélisation}
  
\end{frame}


\section{Expérience proposée}

\subsection{Objectif de la manipulation}

\begin{frame}{Nombre de base par brin et DEP}
    La longueur des brins d'ADN circulant est caractérisé par le nombre de bases.
    Ce nombre de base est relié à plusieurs paramètres de l'état de l'organisme.
    \pause
    \begin{exampleblock}{Problématique}
      Peut-on quantifier rapidement par DEP la distribution de la longueur des brins d'ADN?
    \end{exampleblock}
\end{frame}

\begin{frame}{Problème attendu}
    \pause
    \begin{exampleblock}{Position final de l'ADN}
        Veut-on canaliser l'ADN circulant dans une zone ou la coller à une paroi ?
    \end{exampleblock}
    \pause
    \begin{exampleblock}{Fréquence de travail}
        A quelle fréquence se placer pour ne sélectionner que l'ADN et pas d'autre protéïne ?
    \end{exampleblock}
    \pause
    \begin{exampleblock}{Quelle solution utiliser ?}
        La solution utilisé a un effet sur le comportement de l'ADN
    \end{exampleblock}
\end{frame}

\subsection{Protocole expérimental}

\begin{frame}{Microsystème}
    \begin{figure}[htp]
      \centering
      \includegraphics[width = 0.8 \textwidth]{SVG/Micro-systeme.eps}
      \caption{Schéma du micro-système envisagé}
    \end{figure}
\end{frame}


\subsection{Résultat attendu}



\begin{frame}{Conclusion}
  La diélectrophorèse présente un grand intérêt pour la manipulation des molécules d'ADN sanguines.

  \begin{itemize}
    \item L'analyse de l'ADN peut avoir un intérêt pour des analyses
    \item Il est difficile de caractériser précisément ces molécules
    \item Le comportement de ces molécules ne facilite pas leur analyse
  \end{itemize}

  \pause
  \textbf{$\Rightarrow$ L'expérience proposée semble difficile à réaliser mais présente un grand intérêt scientifique}
\end{frame}
